#include <Ota.h>

#include <config.h>

void Ota::setup(Display& display) {
  ArduinoOTA.setPort(8862);
  ArduinoOTA.setHostname("PopRoaster");
  ArduinoOTA.setPassword(OTA_PASS);

  ArduinoOTA.onProgress([&display](unsigned int progress, unsigned int total) {
    display.show_message("OTA update: " + String(progress / (total / 100)) + "%");
  });

  ArduinoOTA.onError([&display](ota_error_t error) {
    if (error == OTA_AUTH_ERROR)
      display.show_message("Auth Failed");
    else if (error == OTA_BEGIN_ERROR)
      display.show_message("Begin Failed");
    else if (error == OTA_CONNECT_ERROR)
      display.show_message("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR)
      display.show_message("Receive Failed");
    else if (error == OTA_END_ERROR)
      display.show_message("End Failed");

    delay(2000);
  });

  ArduinoOTA.begin();
}
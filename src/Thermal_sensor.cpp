#include <Thermal_sensor.h>

void Thermal_sensor::setup() {
    SPI.begin();
    thermocouple.begin(P_THERMOCOUPLE);
}

void Thermal_sensor::read() {
    if (needs_update())
    {
        const double temp = thermocouple.readCelsius();
        current_temp = std::isnan(temp) ? -1 : temp;

        last_update = millis();
    }
}
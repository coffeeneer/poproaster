#include <Artisan.h>

void Artisan::setup(Display& display) {
    mb.config(WIFI_SSID, WIFI_PASS);

    String dots = "";
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(750);
        dots += ".";
        display.show_message("Connecting " + dots);
    }

    display.show_message("IP: " + WiFi.localIP().toString());
    delay(2000);

    mb.addHreg(REG_BT);
    mb.addHreg(REG_SV);
    mb.addHreg(REG_EN);
    mb.addHreg(REG_AIR);
    mb.addHreg(REG_KP);
    mb.addHreg(REG_KI);
    mb.addHreg(REG_KD);
    mb.addHreg(REG_HEAT);
    mb.addHreg(REG_PID_EN);
}

Roaster_state Artisan::update(Roaster_state state) {
    mb.task();

    // Get
    state.enabled = enabled();
    state.pid_enabled = pid_enabled();
    state.fan_duty = air();
    state.set_temp = sv();
    state.pid_terms = terms();

    // Update
    bt(state.current_temp);
    if (state.pid_enabled) {
        heat(state.heater_duty);
    }
    else
    {
        state.heater_duty = heat();
    }

    return state;
}
#include <Pop_roaster.h>

void Pop_roaster::setup() {
    display.setup();
    display.show_message("Booting...");

    thermal.setup();
    fan.setup();
    heater.setup();
    ota.setup(display);
    artisan.setup(display);
}

void Pop_roaster::update() {
    // Read sensors
    thermal.read();

    // Update state
    state = thermal.update(state);
    state = fan.update(state);
    state = heater.update(state);
    state = artisan.update(state);
    ota.update();

    // Write outputs
    fan.output();
    heater.output();

    // Display state
    display.show_roaster_state(state);
}
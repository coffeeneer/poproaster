#include <Heater.h>

void Heater::setup() {
    pinMode(P_HEATER, OUTPUT);
    digitalWrite(P_HEATER, 0);

    pid.SetMode(MANUAL);
    pid.SetOutputLimits(0.0, 100.0);
}

Roaster_state Heater::update(Roaster_state state) {
  // Update parameters
  set_terms(state.pid_terms);
  set_pid_enabled(state.pid_enabled);
  current_temp = state.current_temp;
  target_temp = state.set_temp;

  // Calculate
  pid.Compute();

  // Update heater duty
  if (pid_enabled) {
    state.heater_duty = duty;
  } else {
    duty = state.heater_duty;
  }

  // Update output
  if (!state.enabled || duty < 1 || state.current_temp > MAX_TEMP) {
    set_on(false);
  }
  else if(duty >= 100.0) {
    set_on(true);
  }
  else if (millis() - last_update >= next_update)
  {
    // Crude slow PWM
    last_update = millis();

    if (!on)
    {
      set_on(true);
      next_update = H_INTERVAL * (duty / (100.0 / H_RESOLUTION));
    }
    else
    {
      set_on(false);
      next_update = H_INTERVAL * ((100.0 - duty) / (100.0 / H_RESOLUTION));
    }
  }

  return state;
}

void Heater::output() {
  if (updated) {
    updated = false;
    digitalWrite(P_HEATER, on);
  }
}
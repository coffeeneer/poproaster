#include <Display.h>

#include <config.h>

void Display::show_roaster_state(Roaster_state state) {
    // Cycle page
    if (millis() - last_page_update >= D_PAGE_INTERVAL) {
        last_page_update = millis();
        state_page = (state_page + 1) % 2;
    }

    if (millis() - last_update >= D_INTERVAL) {
        last_update = millis();

        dp.clearBuffer();
        dp.setFont(u8g2_font_t0_11b_mr);
        dp.drawStr(0, 8, "PopRoaster");

        dp.setFont(u8g2_font_open_iconic_embedded_1x_t);
        char icon[2];
        icon[0] = 78;
        icon[1] = '\0';
        dp.drawStr(64, 8, icon);

        dp.setFont(u8g2_font_t0_11b_mr);
        dp.drawStr(74, 8, state.enabled ? "enabled" : "disabled");

        switch (state_page)
        {
        case 1:
            show_pid_state(state);
            break;

        default:
            show_general_state(state);
            break;
        }

        dp.sendBuffer();
    }
}

void Display::show_message(const String& message) {
    dp.clearBuffer();
    dp.setFont(u8g2_font_t0_11b_mr);
    dp.drawStr(0, 8, "PopRoaster");
    dp.drawStr(0, 20, message.c_str());

    dp.sendBuffer();
}

void Display::show_general_state(Roaster_state state) {
    char buffer[24];
    char icon[2];

    dp.setFont(u8g2_font_open_iconic_gui_1x_t);
    icon[0] = 64;
    icon[1] = '\0';
    dp.drawStr(0, 20, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "BT %.1fC", state.current_temp);
    dp.drawStr(10, 20, buffer);

    dp.setFont(u8g2_font_open_iconic_arrow_1x_t);
    icon[0] = 66;
    icon[1] = '\0';
    dp.drawStr(0, 30, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "PID %s", state.pid_enabled ? "on" : "off");
    dp.drawStr(10, 30, buffer);

    dp.setFont(u8g2_font_open_iconic_other_1x_t);
    icon[0] = 64;
    icon[1] = '\0';
    dp.drawStr(64, 20, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "FAN %d%%", state.fan_duty);
    dp.drawStr(74, 20, buffer);

    dp.setFont(u8g2_font_open_iconic_thing_1x_t);
    icon[0] = 78;
    icon[1] = '\0';
    dp.drawStr(64, 30, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "PWR %d%%", state.heater_duty);
    dp.drawStr(74, 30, buffer);
}

void Display::show_pid_state(Roaster_state state) {
    char buffer[24];
    char icon[2];

    dp.setFont(u8g2_font_open_iconic_gui_1x_t);
    icon[0] = 64;
    icon[1] = '\0';
    dp.drawStr(0, 20, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "P %.2f", state.pid_terms.kp);
    dp.drawStr(10, 20, buffer);

    dp.setFont(u8g2_font_open_iconic_arrow_1x_t);
    icon[0] = 66;
    icon[1] = '\0';
    dp.drawStr(0, 30, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "I %.2f", state.pid_terms.ki);
    dp.drawStr(10, 30, buffer);

    dp.setFont(u8g2_font_open_iconic_other_1x_t);
    icon[0] = 64;
    icon[1] = '\0';
    dp.drawStr(64, 20, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "D %.2f", state.pid_terms.kd);
    dp.drawStr(74, 20, buffer);

    dp.setFont(u8g2_font_open_iconic_thing_1x_t);
    icon[0] = 78;
    icon[1] = '\0';
    dp.drawStr(64, 30, icon);

    dp.setFont(u8g2_font_t0_11b_mr);
    sprintf(buffer, "SV %.1fC", state.set_temp);
    dp.drawStr(74, 30, buffer);
}
#include <Arduino.h>
#include <Pop_roaster.h>

Pop_roaster popr;

void setup() {
  Serial.begin(9600);
  popr.setup();
}

void loop() {
  popr.update();

  delay(1);
}
#include <Fan.h>

#include <config.h>
#include <Arduino.h>

void Fan::setup() {
    analogWriteRange(100);
    pinMode(P_FAN, OUTPUT);
    analogWrite(P_FAN, duty);
}

Roaster_state Fan::update(Roaster_state state) {
    set_duty(state.enabled ? state.fan_duty : 0);

    return state;
}

void Fan::output() {
    if (updated) {
        updated = false;
        analogWrite(P_FAN, duty);
    }
}
#pragma once

#include <config.h>
#include <Arduino.h>
#include <Roaster_state.h>
#include <PID_v1.h>

class Heater {
public:
    Heater(): pid{&current_temp, &duty, &target_temp, terms.kp, terms.ki, terms.kd, DIRECT} {}

    void setup();

    Roaster_state update(Roaster_state state);

    void output();

private:
    Pid_terms terms;
    PID pid;

    bool on;
    bool pid_enabled;
    bool updated;
    double duty;
    double next_update;
    double last_update;
    double current_temp;
    double target_temp;

    void set_terms(Pid_terms val) {
        if (terms != val) {
            terms = val;
            pid.SetTunings(terms.kp, terms.ki, terms.kd);
        }
    }

    void set_pid_enabled(bool val) {
        if (pid_enabled != val) {
            pid_enabled = val;
            pid.SetMode(val ? AUTOMATIC : MANUAL);
        }
    }

    void set_on(bool val) {
        if (on != val) {
            on = val;
            updated = true;
        }
    }
};
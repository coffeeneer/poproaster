#pragma once

#include <Arduino.h>
#include <U8g2lib.h>
#include <Roaster_state.h>

class Display {
public:
    Display(): dp{U8G2_R0} {}

    void setup() {
        dp.begin();
    }

    void show_message(const String& state);

    void show_roaster_state(Roaster_state state);

private:
    U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C dp;
    double last_update;
    double last_page_update;
    int state_page;

    void show_general_state(Roaster_state state);

    void show_pid_state(Roaster_state state);
};
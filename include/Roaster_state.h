#pragma once

#include <config.h>
#include <Pid_terms.h>

struct Roaster_state {
  public:
    Pid_terms pid_terms;

    bool enabled;
    bool pid_enabled;
    int fan_duty;
    int heater_duty;
    double current_temp;
    double set_temp;
};
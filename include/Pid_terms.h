#pragma once

struct Pid_terms {
public:
    Pid_terms(): kp{0}, ki{0}, kd{0} {}
    Pid_terms(double p, double i, double d): kp{p}, ki{i}, kd{d} {}

    double kp;
    double ki;
    double kd;

    bool operator==(const Pid_terms other) const {
        return kp == other.kp && ki == other.ki && kd == other.kd;
    }
    bool operator!=(const Pid_terms other) const {
        return !(*this == other);
    }
};
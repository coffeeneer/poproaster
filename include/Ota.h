#include <ArduinoOTA.h>

#include <Display.h>

class Ota {
  public:
    void setup(Display& display);

    void update() {
      ArduinoOTA.handle();
    }
};
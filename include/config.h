#define P_HEATER D3
#define P_THERMOCOUPLE D0
#define P_FAN D4

#define WIFI_SSID "HH45A - 2.4Ghz"
#define WIFI_PASS "BetterWifiFTW"

#define OTA_PASS "1923876g938176h319287b3987638"

#define USE_HOLDING_REGISTERS_ONLY
#define REG_BT 100
#define REG_SV 200
#define REG_KP 201
#define REG_KI 202
#define REG_KD 203
#define REG_EN 204
#define REG_AIR 205
#define REG_HEAT 206
#define REG_PID_EN 207

#define SLAVE_ID 1

#define S_INTERVAL 250

#define H_INTERVAL 20
#define H_RESOLUTION 50

#define MAX_TEMP 225

#define D_INTERVAL 100
#define D_PAGE_INTERVAL 4000

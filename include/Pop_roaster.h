#pragma once

#include <Fan.h>
#include <Thermal_sensor.h>
#include <Heater.h>
#include <Artisan.h>
#include <Display.h>
#include <Ota.h>
#include <Roaster_state.h>

class Pop_roaster {
private:
    Roaster_state state;

    Display display;
    Ota ota;
    Thermal_sensor thermal;
    Fan fan;
    Heater heater;
    Artisan artisan;

public:
    void setup();

    void update();
};
#pragma once

#include <config.h>
#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <max6675.h>
#include <Roaster_state.h>

class Thermal_sensor {
public:
    Thermal_sensor(): thermocouple() {}

    void setup();

    void read();

    Roaster_state update(Roaster_state state) {
        state.current_temp = current_temp;
        return state;
    }

private:
    MAX6675 thermocouple;
    unsigned long last_update;

    double current_temp;

    bool needs_update() {
        return millis() - last_update >= S_INTERVAL;
    }
};
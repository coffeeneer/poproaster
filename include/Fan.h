#pragma once

#include <Roaster_state.h>

class Fan {
public:
    Fan(): duty{0} {}

    void setup();

    Roaster_state update(Roaster_state state);

    void output();

private:
    int duty;
    bool updated;

    void set_duty(int val) {
        if (duty != val) {
            duty = val;
            updated = true;
        }
    }
};
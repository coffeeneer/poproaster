#pragma once

#include <config.h>
#include <ESP8266WiFi.h>
#include <ModbusIP_ESP8266.h>
#include <Roaster_state.h>
#include <Display.h>

class Artisan {
public:
    void setup(Display& display);

    Roaster_state update(Roaster_state state);

private:
    ModbusIP mb;

    int dti(double val) const {
        return val * 100;
    }

    double itd(int val) const {
        double t = val;
        return t / 100.0;
    }

    bool itb(int val) const {
        return val > 0;
    }

    void bt(double val) {
        mb.Hreg(REG_BT, dti(val));
    }

    Pid_terms terms() {
        return Pid_terms{itd(mb.Hreg(REG_KP)), itd(mb.Hreg(REG_KI)), itd(mb.Hreg(REG_KD))};
    }

    int heat() {
        return mb.Hreg(REG_HEAT);
    }

    void heat(int val) {
        mb.Hreg(REG_HEAT, val);
    }

    double sv() {
        return itd(mb.Hreg(REG_SV));
    }

    int air() {
        return mb.Hreg(REG_AIR);
    }

    bool enabled() {
        return itb(mb.Hreg(REG_EN));
    }

    bool pid_enabled() {
        return itb(mb.Hreg(REG_PID_EN));
    }
};